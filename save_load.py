# %%
# imports
import os

import tensorflow as tf
from tensorflow import keras

print(tf.version.VERSION)

# get dataset
(train_images, train_labels), (test_images, test_labels) = (tf.keras.datasets.
                                                            mnist.load_data())

train_labels = train_labels[:1000]
test_labels = test_labels[:1000]

train_images = train_images[:1000].reshape(-1, 28 * 28) / 255.0
test_images = test_images[:1000].reshape(-1, 28 * 28) / 255.0


# ==============
# Define a model
# ==============
# simple sequential
def create_model():
    model = tf.keras.models.Sequential([
        keras.layers.Dense(512, activation='relu', input_shape=(784,)),
        keras.layers.Dropout(0.2),
        keras.layers.Dense(10)
    ])

    model.compile(optimizer='adam',
                  loss=tf.losses.SparseCategoricalCrossentropy(
                      from_logits=True),
                  metrics=['accuracy'])

    return model


# create basic model instance
model = create_model()

# desplay model architecture
model.summary()

# ================================
# Save checkpoints during training
# ================================
# CALLBACKS
checkpoint_path = "save_load/training_1/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

# create a callback that saves the models weights
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)

# train the model with the new callback
model.fit(train_images,
          train_labels,
          epochs=10,
          validation_data=(test_images, test_labels),
          callbacks=[cp_callback])  # pass callback to training

# This may generate warnings related to saving the state of the optimizer.
# These warnings (and similar warnings throughout this notebook)
# are in place to discourage outdated usage, and can be ignored.

# =================
# Restoring a model
# =================
# create a basic model instance
model = create_model()

# evaluate the model
loss, acc = model.evaluate(test_images, test_labels, verbose=2)
print('Untrained model, accuracy: {:5.2f}%'.format(100*acc))

# load the weights
model.load_weights(checkpoint_path)

# Re-evaluate the model
loss, acc = model.evaluate(test_images, test_labels, verbose=2)
print('Restored model, accuracy: {:5.2f}%'.format(100*acc))

# =====================
# Ckpt callback options
# =====================
# Include the epoch in the file name (uses `str.format`)
checkpoint_path = 'save_load/training_2/cp-{epoch:04d}.ckpt'
checkpoint_dir = os.path.dirname(checkpoint_path)

# Create a callback that saves the model's weights every 5 epochs
cp_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_path,
    verbose=1,
    save_weights_only=True,
    period=5)

# create new model instance
model = create_model()

# save the weights using the `checkpoint_path` format
model.save_weights(checkpoint_path.format(epoch=0))

# train the model with the new callback
model.fit(train_images,
          train_labels,
          epochs=50,
          callbacks=[cp_callback],
          validation_data=(test_images, test_labels),
          verbose=0)

# look at resulting checkpoints
latest = tf.train.latest_checkpoint(checkpoint_dir)
print(latest)

# test by resetting the model and loading the latest checkpoint
# create a new model instance
model = create_model()

# load the previously used weights
model.load_weights(latest)

# re-evaluate the model
loss, acc = model.evaluate(test_images, test_labels, verbose=2)
print("Restored model, accuracy: {:5.2f}%".format(100*acc))

# Manually save weights
# save the weights
model.save_weights('./save_load/my_checkpoints/my_checkpoint')

# create a new model instance
model = create_model()

# restore the weights
model.load_weights('./save_load/my_checkpoints/my_checkpoint')

# evaluate the model
loss, acc = model.evaluate(test_images, test_labels, verbose=2)
print("Restored model, accuracy: {:5.2f}%".format(100*acc))

# =====================
# Save the entire model
# =====================
# create and train a new model instance
model = create_model()
model.fit(train_images, train_labels, epochs=5)

# save the entire model as a SavedModel
tf.saved_model.save(model, './save_load/saved_model/my_model')

# reload a fresh keras model from the saved model
new_model = tf.keras.models.load_model('./save_load/saved_model/my_model')

# check its architecture
new_model.summary()

# evaluate the restored model
loss, acc = new_model.evaluate(test_images, test_labels, verbose=2)
print('Restored model, accuracy: {:5.2f}%'.format(100*acc))

print(new_model.predict(test_images).shape)
# %%
