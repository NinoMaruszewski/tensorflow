# imports
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers

import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

print(tf.__version__)

# ================
# Auto MPG Dataset
# ================
# download
dataset_path = keras.utils.get_file("auto-mpg.data",
                                    ("http://archive.ics.uci.edu/ml/machine-le"
                                     "arning-databases/auto-mpg/auto-mpg.data")
                                    )
print(dataset_path)

# import dataset w/ pandas
column_names = ['MPG', 'Cylinders', 'Displacement', 'Horsepower', 'Weight',
                'Acceleration', 'Model Year', 'Origin']
dataset = pd.read_csv(dataset_path, names=column_names,
                      na_values="?", comment='\t',
                      sep=" ", skipinitialspace=True)


print(dataset.tail())

# clean data
print(dataset.isna().sum())
dataset = dataset.dropna()

dataset['Origin'] = dataset['Origin'].map({1: 'USA', 2: 'Europe', 3: 'Japan'})
dataset = pd.get_dummies(dataset, prefix='', prefix_sep='')
print(dataset.tail())

# ====================
# Split and Train data
# ====================
train_dataset = dataset.sample(frac=0.8, random_state=0)
test_dataset = dataset.drop(train_dataset.index)

# inspect data
sns.pairplot(train_dataset[["MPG", "Cylinders", "Displacement", "Weight"]],
             diag_kind='kde')
plt.show()

train_stats = train_dataset.describe()
print(train_stats.pop("MPG"))
train_stats = train_stats.transpose()
print(train_stats)

# separate target value/label from data
train_labels = train_dataset.pop('MPG')
test_labels = test_dataset.pop('MPG')


# Normalize data
def norm(x):
    return (x - train_stats['mean']) / train_stats['std']


normed_train_data = norm(train_dataset)
normed_test_data = norm(test_dataset)


# =========
# the model
# =========
# build the model
def build_model():
    model = keras.Sequential([
        layers.Dense(64, activation='relu',
                     input_shape=[len(train_dataset.keys())]),
        layers.Dense(64, activation='relu'),
        layers.Dense(1)
    ])

    optimizer = tf.keras.optimizers.RMSprop(0.001)

    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae', 'mse'])
    return model


model = build_model()

# inspect model
print(model.summary())

example_batch = normed_train_data[:10]
example_result = model.predict(example_batch)
print(example_result)

# train the model
EPOCHS = 1000

# the patience parameter is the amount of epochs to check for improvement
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

early_history = model.fit(normed_train_data, train_labels,
                          epochs=EPOCHS, validation_split=0.2, verbose=0,
                          callbacks=[early_stop, tfdocs.modeling.EpochDots()])

# history = model.fit(
#     normed_train_data, train_labels,
#     epochs=EPOCHS, validation_split=0.2, verbose=0,
#     callbacks=[tfdocs.modeling.EpochDots()])

# view data
# hist = pd.DataFrame(history.history)
# hist['epoch'] = history.epoch
# print(hist.tail())

# # plot it
# # this is for full data
plotter = tfdocs.plots.HistoryPlotter(smoothing_std=2)
# plotter.plot({'Basic': history}, metric='mae')
# plt.ylim([0, 10])
# plt.ylabel('MAE [MPG]')
# plt.show()

# plotter.plot({'Basic': history}, metric='mse')
# plt.ylim([0, 20])
# plt.ylabel('MSE [MPG^2]')
# plt.show()

# this is for early stop
plotter.plot({'Early Stopping': early_history}, metric='mae')
plt.ylim([0, 10])
plt.ylabel('MAE [MPG]')
plt.show()

# test the model with the test dataset
loss, mae, mse = model.evaluate(normed_test_data, test_labels, verbose=2)

print("Testing set Mean Abs Error: {:5.2f} MPG".format(mae))

# finally, make predictions
test_predictions = model.predict(normed_test_data).flatten()

a = plt.axes(aspect='equal')
plt.scatter(test_labels, test_predictions)
plt.xlabel('True Values [MPG]')
plt.ylabel('Predictions [MPG]')
lims = [0, 50]
plt.xlim(lims)
plt.ylim(lims)
_ = plt.plot(lims, lims)
plt.show()

error = test_predictions - test_labels
plt.hist(error, bins=25)
plt.xlabel("Prediction Error [MPG]")
_ = plt.ylabel("Count")
plt.show()
