# imports
# import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
import tensorflow_datasets as tfds

print("Version: ", tf.__version__)
print("Eager mode: ", tf.executing_eagerly())
print("Hub version: ", hub.__version__)
print("GPU is", "available"
      if tf.config.experimental.list_physical_devices("GPU")
      else "NOT AVAILABLE")

# Split the training set 60/40, so we'll end up with 15,000 examples
# for training, 10,000 for validation and 25,000 for testing
train_data, validation_data, test_data = tfds.load(
    name='imdb_reviews',
    split=[
        tfds.Split.TRAIN.subsplit(tfds.percent[:60]),
        tfds.Split.TRAIN.subsplit(tfds.percent[60:]),
        tfds.Split.TEST
    ],
    as_supervised=True)

# Explore data
train_examples_batch, train_labels_batch = next(iter(train_data.batch(10)))
print(train_examples_batch)
print(train_labels_batch)

# ===============
# build the model
# ===============
# text embedding
embedding = "https://tfhub.dev/google/tf2-preview/gnews-swivel-20dim/1"
hub_layer = hub.KerasLayer(embedding, input_shape=[],
                           dtype=tf.string, trainable=True)
print(hub_layer(train_examples_batch[:3]))

# full model
model = tf.keras.Sequential()
model.add(hub_layer)
model.add(tf.keras.layers.Dense(16, activation='relu'))
model.add(tf.keras.layers.Dense(1))

model.summary()

# loss fn
model.compile(optimizer='adam',
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])

model.fit(train_data.shuffle(1000).batch(512),
          epochs=20,
          validation_data=validation_data.batch(512),
          verbose=1)

# ==============
# Evaluate model
# ==============
results = model.predict(test_data.batch(512), verbose=2)

for name, value in zip(model.metrics_names, results):
    print("%s: %.3f" % (name, value))
